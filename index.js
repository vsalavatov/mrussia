var $ = require('jquery');
var Raphael = require('raphael');

// TMP
var map, board, amount, current, currentElem, currentName, score, panzoom, hoverTimer, langAttr, mapName,
    FILL = "fill", STARTED = "STARTED", FINISHED = "FINISHED", 
    REGIONS = "regions", CAPITALS = "capitals", FLAGS = "flags", 
    resultMessage = $("#resultMessage"), resultCaption = $("#resultCaption"), enableZoom = $("#enableZoom"), 
    hoverError = $("#hoverError"), hoverCorrect = $("#hoverCorrect"), 
    baseColour = "#eee", hintColour = "#eee", faceColour = "#eee", niceColour = "#2e2",
    game_regions = [], empty_regions = {}, slang = {}, colourCorrect = !0, showRegionName = !0;

var pos = {
    x: 0,
    y: 0
};
function regionDown(e) {
    console.log(e);
    pos.x = e.pageX,
    pos.y = e.pageY;
    console.log(pos.x, pos.y);
    
    /*
    var circle = map.circle(13, 13, 10.5);
    circle.attr("fill", "green");
    //circle.attr("stroke-width", 2);
    var text = map.text(13, 13, "Magic");
    text.attr({'font-size': 15, 'font-family': 'FranklinGothicFSCondensed-1, FranklinGothicFSCondensed-2'});
    text.attr("fill", "#f1f1f1");

    var set = map.set(
    (circle),
    );

    set.transform('t' + (e.pageX / 2) + ',' + (e.pageY / 2));

    set = map.set((text));

    set.transform('t' + (e.pageX / 2) + ',' + (e.pageY / 2 + 20));    

    circle.click(function(e) {
      alert('Clicked!');
    })
    */
    
    $(".modal-header").text(this.data('object'));
    modal.style.display = "block";
}
function regionUp(e) {
    enableZoom.prop("checked") ? Math.abs(pos.x - e.pageX) < 10 && Math.abs(pos.y - e.pageY) < 10 : 0
}

function raiseHoverError(e, r) {
    hoverCorrect.hide(),
    raiseHover(hoverError, e, r)
}
function raiseHoverCorrect(e, r) {
    hoverError.hide(),
    raiseHover(hoverCorrect, e, r)
}
function raiseHover(e, r, t) {
    hoverCorrect.hide(),
    hoverError.hide(),
    e.show(),
    clearTimeout(hoverTimer),
    hoverTimer = setTimeout(function() {
        e.hide()
    }, 1523),
    e.css("left", r.pageX + 10),
    e.css("top", r.pageY + 10),
    e.html(t)
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}


$.getJSON("map.json", function(e) {
  function r(e) {
        var r, t = map.path(e.src);
        switch (r = e.subclass ? e.subclass : "region",
        t.attr("class", r),
        r) {
        case "region":
        case "waters":
            t.data({
                capital: e.capital,
                capital_params: e.capital_params,
                name: e.name,
                object: e.object,
                params: e.object_params
            });
            e.capital === e.object && t.data("city-state", !0);

            var tmp = ["Bashkortostan"];
            
            if (tmp.includes(e.name)) {
                t.attr(FILL, niceColour);
            } else {
                t.attr(FILL, baseColour);
            }
            t.mouseup(regionUp);
            t.mousedown(regionDown);
            game_regions.push(t);
            break;
        case "dashed":
            t.toFront();
            break;
        case "empty":
            empty_regions[e.name] = t,
            t.toBack();
            break;
        case "zoom":
            t.toBack()
        }
    }
  var t = e.result;
  if (t) {
      var o = t.settings;
      map = function(e, r) {
          var t = Raphael("map", e, r);
          t.setViewBox(0, 0, e, r, !0);
          var o = document.querySelector("svg");
          return o.removeAttribute("width"),
          o.removeAttribute("height"),
          t
      }(o.width, o.height);
      for (var i = 0; i < t.paths.length; i++)
          r(t.paths[i]);
  }
})


var modal = document.getElementById('info');

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}